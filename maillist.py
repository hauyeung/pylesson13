'''
Created on Jun 25, 2014

@author: hauyeung
'''
import mysql.connector, datetime, settings
from database import login_info
def genmaillist():
    db = mysql.connector.Connect(**login_info)
    cursor = db.cursor()
    
    cursor.execute("""DROP TABLE IF EXISTS maillist""")
    cursor.execute("""
        CREATE TABLE maillist (
            id INT PRIMARY KEY AUTO_INCREMENT,
            emaildate DATETIME,
            fromemail VARCHAR(100),
            toemail VARCHAR(100),
            message VARCHAR(200))        
        """)
    
    messages = genmsgs(settings.recipients, settings.starttime, settings.daycount)
    for x in messages:
        cursor.execute("""INSERT INTO maillist (id, emaildate, 
        fromemail, toemail, message)
        VALUES (%s, %s, %s, %s, %s)""", ('',x[0],x[1], x[2], x[3]))
        print(x)
        db.commit()

    db.close()
    return messages
    
def genmsgs(recipients, starttime, daycount):
    mailmsgs = []
    for d in range(daycount):
        dt = starttime + datetime.timedelta(days = d)
        for r in recipients:
            mailmsgs.append((dt,'test@example.com', r,'This is a test.'))
            
    return mailmsgs


        
        