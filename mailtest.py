'''
Created on Jun 25, 2014

@author: hauyeung
'''
import unittest, mysql.connector, maillist, datetime
from database import login_info


class Test(unittest.TestCase):


    def setUp(self):
        self.db = mysql.connector.Connect(**login_info)
        self.cursor = self.db.cursor()
        self.cursor.execute('DROP TABLE IF EXISTS maillist')
        mlist = maillist.genmaillist()
        self.correctstart = mlist[0]
        self.correctend = mlist[-1] 
        self.maillistlength = len(mlist)

    def test_emailist(self):
        self.cursor.execute("""SELECT emaildate, fromemail, 
        toemail, message FROM maillist""")
        msglist = self.cursor.fetchall()
        self.assertEqual(msglist[0], self.correctstart)
        self.assertEqual(msglist[-1], self.correctend)
        
    def test_emailnum(self):
        self.cursor.execute("SELECT COUNT(*) FROM maillist")
        numemails = self.cursor.fetchone()
        self.assertEqual(self.maillistlength, numemails[0])
    
    def test_emptymsg(self):
        self.cursor.execute("""INSERT INTO maillist (emaildate, fromemail, toemail) 
        VALUES (%s, %s, %s)""",
        (datetime.datetime(2014,12,31,1,0,0),
        'test@example.com',
        'bad@example.com'))
        self.cursor.execute('''SELECT * FROM maillist 
        WHERE message IS NULL''')        
        badmsg = self.cursor.fetchall()
        for a in badmsg:
            print(str(a[1])+' has no message.')
        self.assertEqual(1,len(badmsg))
        for a in badmsg:
            self.cursor.execute('DELETE FROM maillist WHERE id='+str(a[0]))            
        
                         
    def tearDown(self):
        self.cursor.execute('drop table if exists maillist')
        self.db.close()


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()